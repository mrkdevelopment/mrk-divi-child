/*
Theme Name:  <?php echo $Divi_child_name, "\n"; ?>
Theme URI:   <?php echo $Divi_child_uri, "\n"; ?>
Version:     <?php echo $Divi_child_version, "\n"; ?>
Description: <?php echo $Divi_child_description, "\n"; ?>
Author:      <?php echo $Divi_child_authorname, "\n"; ?>
Author URI:  <?php echo $Divi_child_authoruri, "\n"; ?>
Template:    Divi
*/


/**************/
/* CUSTOM MRK */
/**************/


/*** GRAVITY FORM STYLING ***/

input[type=text], input.text, input.title, textarea, select {
    color: #4e4e4e;
}

.gform_wrapper .top_label .gfield_label {
    margin: .625em 0 .5em;
    font-weight: 700;
    display: -moz-inline-stack;
    display: inline-block;
    line-height: 1.3;
    clear: both;
}
.ginput_container .textarea, .ginput_container input[type="text"], .ginput_container input[type="password"] {
    border-radius: 3px;
    height: 35px;
    padding: 5px 5px !important;
    background: #F8F8F8;
    border: 1px solid #B7B7B7;
}
.gform_previous_button, .gform_next_button, .gform_button {
    color: #ffffff;
    cursor: pointer;
    display: inline-block;
    line-height: 35px;
    background: #0079C5;
    padding: 0px 20px;
    vertical-align: middle;
    font-size: 18px;
    min-width: 80px;
    min-height: 35px;
    border: 3px solid #ffffff;
    margin: 5px !important;
    border-radius: 5px;
}

/***********************************************************************
Caution: do not remove this to keep Divi Children Engine always updated 
Divi Children Engine version: 1.0.3
***********************************************************************/

/*------------------------------[FONT ICONS]-----------------------------*/
/*--[Created by Divi Children, http://divi4u.com/divi-children-plugin/]--*/
/*-----------------------------------------------------------------------*/

.icon_tags, .icon_profile, .icon_chat, .icon_clipboard, .icon_calendar {
	font-family: 'ETmodules';
	speak: none;
	font-style: normal;
	font-weight: normal;
	font-variant: normal;
	text-transform: none;
	line-height: 1;
	-webkit-font-smoothing: antialiased;
	-moz-osx-font-smoothing: grayscale;
	font-size: 16px;
	color: #318EC3;
}
.icon_tags:before {
	content: "\e07c";
	padding: 0px 5px 0 0;
}
.icon_profile:before {
	content: "\e08a";
	padding: 0 5px 0 0;
}
.icon_chat:before {
	content: "\e066";
	padding: 0 5px 0 20px;
}
.icon_clipboard:before {
	content: "\e0e6";
	padding: 0 5px 0 20px;
}
.icon_calendar:before {
	content: "\e023";
	padding: 0 5px 0 20px;
}

/*- YOU CAN INCLUDE THE CUSTOM CODE FOR YOUR CHILD THEME BELOW THIS LINE-*/

/*------------------------------------------------*/
/*-----------------[BASIC STYLES]-----------------*/
/*------------------------------------------------*/



/*------------------------------------------------*/
/*---------------[MAIN LAYOUT INFO]---------------*/
/*------------------------------------------------*/



/*------------------------------------------------*/
/*---------------[DROPDOWN MENU]------------------*/
/*------------------------------------------------*/



/*------------------------------------------------*/
/*-------------------[COMMENTS]-------------------*/
/*------------------------------------------------*/



/*------------------------------------------------*/
/*---------------------[FOOTER]-------------------*/
/*------------------------------------------------*/



/*------------------------------------------------*/
/*--------------------[SIDEBAR]-------------------*/
/*------------------------------------------------*/



/*------------------------------------------------*/
/*------------[CAPTIONS AND ALIGNMENTS]-----------*/
/*------------------------------------------------*/



/*------------------------------------------------*/
/*-----------------[WP CALENDAR]------------------*/
/*------------------------------------------------*/



/*------------------------------------------------*/
/*-------------------[PAGE NAVI]------------------*/
/*------------------------------------------------*/



/*------------------------------------------------*/
/*------------------[TRANSITIONS]-----------------*/
/*------------------------------------------------*/



/*------------------------------------------------*/
/*---------------[RESPONSIVE LAYOUTS]-------------*/
/*------------------------------------------------*/

@media only screen and ( min-width: 981px ) {

}

/*-------------------[960px]------------------*/
@media only screen and ( min-width: 981px ) and ( max-width: 1100px ) {

	/*-------------------[Sliders]------------------*/

	/*-------------------[Portfolio]------------------*/

	/*-------------------[Sidebar]------------------*/
	
	/*-------------------[Menu]------------------*/
	
}

/*-------------------[768px]------------------*/
@media only screen and ( max-width: 980px ) {

	/*-------------------[Portfolio]------------------*/
	
	/*-------------------[Sliders]------------------*/
	
	/*-------------------[Menu]------------------*/

}

@media only screen and ( min-width: 768px ) and ( max-width: 980px ) {

}

@media screen and ( max-width: 782px ) {

}

/*-------------------[480px]------------------*/
@media only screen and ( max-width: 767px ) {

	/*-------------------[Menu]------------------*/

}

/*-------------------[320px]------------------*/
@media only screen and ( max-width: 479px ) {

}