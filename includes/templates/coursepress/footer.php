<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package CoursePress
 */
?>

<?php if ( 'on' == et_get_option( 'divi_back_to_top', 'false' ) ) : ?>

	<span class="et_pb_scroll_top et-pb-icon"></span>

<?php endif; ?>

</div><!-- #content -->

<div class="push"></div>
</div><!-- #page -->


			<?php
			if ( has_nav_menu( 'footer-menu' ) ) : ?>


			<footer id="main-footer" class="site-footer" role="contentinfo">
			    <?php
/*
			    <nav id="footer-navigation" class="footer-navigation wrap" role="navigation">
			        <?php wp_nav_menu( array( 'theme_location' => 'secondary' ) ); ?>
			    </nav><!-- #site-navigation -->

				<?php get_sidebar( 'footer' ); ?>
*/
			    ?>

				<div id="et-footer-nav">
					<div class="container">
						<?php
							wp_nav_menu( array(
								'theme_location' => 'footer-menu',
								'depth'          => '1',
								'menu_class'     => 'bottom-nav',
								'container'      => '',
								'fallback_cb'    => '',
							) );
						?>
					</div>
				</div> <!-- #et-footer-nav -->
			</footer> <!-- #main-footer -->


			<?php endif; ?>

			<div id="footer-bottom">
				<div class="container clearfix">
					
			<?php
				if ( false !== et_get_option( 'show_footer_social_icons', true ) ) {
					get_template_part( 'includes/social_icons', 'footer' );
				}
			?>

					<p id="footer-info"><?php echo Divichild_footer_credits_generator(); ?></p>
				</div>	<!-- .container -->
			</div>
			


<?php wp_footer(); ?>

</body>
</html>