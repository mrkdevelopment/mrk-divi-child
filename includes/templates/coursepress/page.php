<?php
/*
Template Name: Default ( footer widgets )
*/

/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package CoursePress
 */

get_header(); ?>

<?php

if (MRK_CP_DEBUG) echo 'PAGE';

global $post;

$unitname = get_query_var('unitname');
if ($unitname != '') {

	global $mrk_cp_pages, $wp_query;

	$pageslug = 'unit';
	$pagename = 'Unit: '.ucfirst($unitname);

	$template = dirname( __FILE__ ) . '/content-page.php';
	remove_filter( 'the_content', 'wpautop' );

	ob_start();

	while ( have_posts() ) : the_post();
		get_template_part( 'content', 'page' );
		// If comments are open or we have at least one comment, load up the comment template
		if ( ( comments_open() || '0' != get_comments_number() ) && $post->comment_status !== 'closed' ) :
			comments_template();
		endif;
	endwhile;

	$content_template = ob_get_clean();

	$args = array(
	    'slug'       => $pageslug,
	    'title'      => __( ucfirst($pagename), 'cp' ),
	    'content'    => $content_template,
	    'type'       => 'virtual_page'
	);

	$virtualpage = new CoursePress_Virtual_Page( $args );

	set_transient('mrk_coursepress_'.$pageslug.'_content', $virtualpage, 60*60);


	// CALL DIVI PAGE
	$location = home_url() . '/index.php?page_id='.$mrk_cp_pages[$pageslug];
	wp_redirect( $location );
	exit;
} else {

?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template
					if ( ( comments_open() || '0' != get_comments_number() ) && $post->comment_status !== 'closed' ) :
						comments_template();
					endif;
				?>

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
}
?>

<?php get_sidebar( 'footer' ); ?>
<?php get_footer(); ?>