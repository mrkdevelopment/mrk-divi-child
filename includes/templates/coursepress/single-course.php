
<?php if (MRK_CP_DEBUG) echo 'SINGLE COURSE FILE'; ?>


		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'content-course-overview', 'single' ); ?>

			<?php coursepress_post_nav(); ?>

		<?php endwhile; // end of the loop. ?>

