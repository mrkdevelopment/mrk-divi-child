<?php
/**
 * The units archive template file
 *
 * @package CoursePress
 */

global $mrk_cp_pages, $wp_query;

$fileslug = 'unit';
$pagename = 'units';

$template = dirname( __FILE__ ) . '/archive-'.$fileslug.'_mrk.php';
remove_filter( 'the_content', 'wpautop' );

ob_start();
include_once( $template );
$content_template = ob_get_clean();

$args = array(
    'slug'       => $pagename,
    'title'      => __( ucfirst($pagename), 'cp' ),
    'content'    => $content_template,
    'type'       => 'virtual_page'
);

$virtualpage = new CoursePress_Virtual_Page( $args );

set_transient('mrk_coursepress_'.$pagename.'_content', $virtualpage, 60*60);


// CALL DIVI PAGE
$location = home_url() . '/index.php?page_id='.$mrk_cp_pages[$pagename];
wp_redirect( $location );
exit;

